package com.test.ecommerce.repository;

import com.test.ecommerce.models.Orders;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepository extends CrudRepository<Orders, Long> {
    List<Orders> findByUserId(Long userId);
}
