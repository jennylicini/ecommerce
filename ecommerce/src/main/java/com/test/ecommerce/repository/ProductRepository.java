package com.test.ecommerce.repository;

import com.test.ecommerce.models.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long> {

}
