
package com.test.ecommerce.repository;

import com.test.ecommerce.models.OrderDetail;
import org.springframework.data.repository.CrudRepository;


public interface OrderDetailRepository extends CrudRepository<OrderDetail, Long> {


}

