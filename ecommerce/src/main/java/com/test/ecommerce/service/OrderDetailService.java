
package com.test.ecommerce.service;

import com.test.ecommerce.models.OrderDetail;

public interface OrderDetailService {

    OrderDetail saveOrderDetail(OrderDetail orderDetail);

}

