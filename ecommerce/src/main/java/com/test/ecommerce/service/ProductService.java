package com.test.ecommerce.service;

import com.test.ecommerce.models.Product;

import java.util.List;

public interface ProductService {

    List<Product> getAll();
    Product findById(Long id);

}
