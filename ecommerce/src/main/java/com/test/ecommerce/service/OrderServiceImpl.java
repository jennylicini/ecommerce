package com.test.ecommerce.service;

import com.test.ecommerce.models.OrderDetail;
import com.test.ecommerce.models.Orders;
import com.test.ecommerce.repository.OrderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    private OrderRepository orderRepository;

    public OrderServiceImpl(OrderRepository orderDetailRepository) {
        this.orderRepository = orderDetailRepository;
    }


    @Transactional
    public Orders saveOrder(Orders orders) {
        return orderRepository.save(orders);
    }

    public List<Orders> getOrdersByUser(Long id) {
        return orderRepository.findByUserId(id);
    }
}
