package com.test.ecommerce.service;

import com.test.ecommerce.models.OrderDetail;
import com.test.ecommerce.repository.OrderDetailRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class OrderDetailServiceImpl implements OrderDetailService{

    private OrderDetailRepository orderDetailRepository;

    public OrderDetailServiceImpl(OrderDetailRepository orderDetailRepository) {
        this.orderDetailRepository = orderDetailRepository;
    }


    @Transactional
    public OrderDetail saveOrderDetail(OrderDetail orderDetail) {
        OrderDetail orderdetailresponse = orderDetailRepository.save(orderDetail);
        return orderdetailresponse;
    }
}
