package com.test.ecommerce.controllers;

import com.test.ecommerce.models.*;

import com.test.ecommerce.repository.UserRepository;
import com.test.ecommerce.service.*;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpSession;
import java.util.*;


@Controller
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderDetailService orderDetailService;

    @Autowired
    UserRepository userRepository;

    private static final int EXPIRATION_TIME = 600; //600
    private long LAST_PRODUCT = 0;
    private int NEW_EXPIRATION_TIME = 0;

    public ProductController(ProductService productService, OrderService orderService){

        this.productService = productService;
        this.orderService = orderService;
    }


    @RequestMapping("/ecommerce")
    public String index(Model model, HttpSession session){

        TreeMap<Integer, Product> cart = (TreeMap<Integer, Product>) session.getAttribute("sessioncart");



        updateTime(session, cart, false);
        List<Product> products = productService.getAll();


        model.addAttribute("products", products)
        .addAttribute("minutes", getMinutes())
        .addAttribute("seconds", getSeconds());

        return "ecommerce";
    }

    @RequestMapping(value =  "ecommerce/addProductCart/{id}", method = RequestMethod.GET)
    public String addProductCart(@PathVariable("id") Long id, HttpSession session){

        Product product = productService.findById(id);
        LAST_PRODUCT = System.currentTimeMillis();

        if (session.getAttribute("sessioncart") == null) {
            TreeMap<Integer, Product> cart = new TreeMap<>();
            cart.put(1, product);
            session.setAttribute("sessioncart", cart);
        }else{
            TreeMap<Integer, Product> cart = (TreeMap<Integer, Product>) session.getAttribute("sessioncart");
            int key = 0;
            if (cart.lastEntry() == null){
                key = 1;
            }else{
                key = cart.lastEntry().getKey()+1;
            }
            cart.put(key, product);
            session.setAttribute("sessioncart", cart);
        }
        return "redirect:/ecommerce/cart";

    }

    @RequestMapping("ecommerce/cart")
    public String Cart(Model model, HttpSession session){
        try {
            TreeMap<Integer, Product> cart = (TreeMap<Integer, Product>) session.getAttribute("sessioncart");
            updateTime(session, cart, false);
            if (cart != null) {
                if (cart.isEmpty()) return "redirect:/ecommerce/emptycart";
                model.addAttribute("cart", cart)
                        .addAttribute("tot", totCart(cart))
                        .addAttribute("minutes", getMinutes())
                        .addAttribute("seconds", getSeconds());
                return "/cart";

            }
            else {
                return "redirect:/ecommerce/emptycart";
            }
        }
            catch (IllegalStateException | NullPointerException e){
                return "redirect:/ecommerce/emptycart";
        }

    }

    @RequestMapping(value = "/deleteProductCart/{key}", method = RequestMethod.GET)
    public String deleteProductCart(@PathVariable("key") int key, HttpSession session, Model model){

        TreeMap<Integer, Product> cart = (TreeMap<Integer, Product>) session.getAttribute("sessioncart");
        cart.remove(key);
        model.addAttribute("cart", cart)
                .addAttribute("tot", totCart(cart));

        return "redirect:/ecommerce/cart";
    }

    @RequestMapping("ecommerce/emptycart")
    public String emptyCart(){
        return "emptycart";
    }

    @RequestMapping("ecommerce/payment")
    public String payment(HttpSession session, @AuthenticationPrincipal UserDetails user){
        TreeMap<Integer, Product> cart = (TreeMap<Integer, Product>) session.getAttribute("sessioncart");

        //get today date
        Date date = new Date();
        java.sql.Date sql_date = convertDate(date);

        //get info User
        User appUser = userRepository.findByUsername(user.getUsername()).orElseThrow(() -> new UsernameNotFoundException("No existe usuario"));

        //create order
        Orders order = new Orders();
        order.setDate(sql_date);
        order.setUser(appUser);
        orderService.saveOrder(order);


      for(Map.Entry<Integer, Product> entry : cart.entrySet()) {
            Product product = entry.getValue();

          OrderDetail orderDetail = new OrderDetail();
            orderDetail.setProduct(product);
            orderDetail.setOrders(order);

            orderDetailService.saveOrderDetail(orderDetail);
        }

        updateTime(session, cart, true);
        return "payment";
    }


    @RequestMapping("ecommerce/user")
    public String user(Model model, HttpSession session,  @AuthenticationPrincipal UserDetails user){
        TreeMap<Integer, Product> cart = (TreeMap<Integer, Product>) session.getAttribute("sessioncart");
        updateTime(session, cart, false);

        User appUser = userRepository.findByUsername(
                user.getUsername()).orElseThrow(() -> new UsernameNotFoundException("No existe usuario"));


        List<Orders> orders = orderService.getOrdersByUser(appUser.getId());



        model.addAttribute("orders", orders)
        .addAttribute("minutes", getMinutes())
                .addAttribute("seconds", getSeconds());

        return "user";
    }

    private Double totCart(TreeMap<Integer, Product> cart) {
        Double tot = 0.0;
        for (Product p : cart.values()) {
            tot += p.getAmount();
        }
        return tot;
    }

    private void updateTime(HttpSession session, TreeMap cart, boolean delete) {
        long currentTime = System.currentTimeMillis();

        if (LAST_PRODUCT != 0) {
            NEW_EXPIRATION_TIME = EXPIRATION_TIME - (int) ((currentTime-LAST_PRODUCT)/1000);

            if (NEW_EXPIRATION_TIME <= 0 || cart == null || cart.isEmpty() || delete) {
                session.setAttribute("sessioncart", null);
                NEW_EXPIRATION_TIME = 0;
            }
        }

    }
    private int getSeconds() {
        return NEW_EXPIRATION_TIME % 60;
    }

    private int getMinutes() {
        return (NEW_EXPIRATION_TIME % 3600) / 60;
    }

    private java.sql.Date convertDate(Date date) {
        java.sql.Date sqlDate = new java.sql.Date(date.getTime());
        return  sqlDate;
    }
}
